# README #

Revolut Android test application made by Moklyakov Denis
[https://docs.google.com/document/d/13Ecs3hhgZJJLsugNUwZPUn_9gsqzwH80Bb-1CRbauTQ]

### Short description ###

* Java 8
* MVP
* Only portrait screen orientation
* Cool flag icons

### Used libraries ###

* AndroidX v1.0.0
* Retrofit v2.4.0
* GSON v2.8.5
* rxJava v2.2.0

