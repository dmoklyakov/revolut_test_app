package self.dmoklyakov.revolut_test_app.common.utils;

import android.content.Context;
import android.content.res.Resources;

import androidx.annotation.IntegerRes;
import self.dmoklyakov.revolut_test_app.R;

public class ResourceUtils {

    public static int getFlagResourceIdByCurrencyCode(Context context, String currencyCode) {
        Resources resources = context.getResources();
        @IntegerRes int resId = resources.getIdentifier("ic_flag_" + currencyCode.toLowerCase(), "drawable", context.getPackageName());
        return resId == 0 ? R.drawable.ic_flag_unknown : resId;
    }

    public static int getCurrencyNameResourceIdByCurrencyCode(Context context, String currencyCode) {
        Resources resources = context.getResources();
        @IntegerRes int resId = resources.getIdentifier("currency_" + currencyCode.toLowerCase(), "string", context.getPackageName());
        return resId == 0 ? R.string.currency_unknown : resId;
    }

}
