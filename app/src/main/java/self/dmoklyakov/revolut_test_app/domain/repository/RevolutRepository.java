package self.dmoklyakov.revolut_test_app.domain.repository;

import self.dmoklyakov.revolut_test_app.RevolutTestApplication;

public class RevolutRepository {

    private static RevolutRepository revolutRepository;

    private RevolutService revolutService;

    private RevolutRepository(RevolutTestApplication application) {
        revolutService = application.getRetrofit().create(RevolutService.class);
    }

    public static RevolutRepository getInstance(RevolutTestApplication application) {

        if (revolutRepository == null) {
            revolutRepository = new RevolutRepository(application);
        }
        return revolutRepository;
    }

    public RevolutService getRevolutService() {
        return revolutService;
    }
}
