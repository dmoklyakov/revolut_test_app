package self.dmoklyakov.revolut_test_app.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.TreeMap;

public class ExchangeRatesResponse {

    @Expose
    @SerializedName("base")
    private String base;

    @Expose
    @SerializedName("date")
    private String date;

    @Expose
    @SerializedName("rates")
    private TreeMap<String, String> rates;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public TreeMap<String, String> getRates() {
        return rates;
    }

    @Override
    public String toString() {
        return "ExchangeRatesResponse{" +
                "base='" + base + '\'' +
                ", date='" + date + '\'' +
                ", rates=" + rates +
                '}';
    }
}
