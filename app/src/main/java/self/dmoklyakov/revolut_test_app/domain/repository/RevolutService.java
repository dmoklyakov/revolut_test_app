package self.dmoklyakov.revolut_test_app.domain.repository;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import self.dmoklyakov.revolut_test_app.domain.model.ExchangeRatesResponse;

public interface RevolutService {

    @GET("latest")
    Observable<ExchangeRatesResponse> getLatestExchangeRates(@Query("base") String base);

}
