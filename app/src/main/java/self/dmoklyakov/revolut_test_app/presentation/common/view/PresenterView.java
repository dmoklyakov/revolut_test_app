package self.dmoklyakov.revolut_test_app.presentation.common.view;

import android.content.Context;

import androidx.annotation.NonNull;

public interface PresenterView {

    @NonNull
    Context getContext();

}
