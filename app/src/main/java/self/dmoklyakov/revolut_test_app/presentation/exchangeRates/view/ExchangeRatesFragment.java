package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import self.dmoklyakov.revolut_test_app.R;
import self.dmoklyakov.revolut_test_app.common.utils.KeyboardUtils;
import self.dmoklyakov.revolut_test_app.presentation.common.view.AbstractFragmentView;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.presenter.ExchangeRatesPresenter;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.presenter.ExchangeRatesPresenterImpl;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel.ExchangeRateViewModel;

public class ExchangeRatesFragment extends AbstractFragmentView implements ExchangeRatesView, ExchangeRatesAdapter.OnItemSelectedListener {

    private RecyclerView rvExchangeRates;
    private ProgressBar pbExchangeRates;

    private ExchangeRatesAdapter exchangeRatesAdapter;

    private TextWatcher textWatcher;

    private Handler handler;

    public static ExchangeRatesFragment newInstance() {
        ExchangeRatesFragment fragment = new ExchangeRatesFragment();
        fragment.presenter = new ExchangeRatesPresenterImpl(fragment);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exchange_rates, container, false);
        rvExchangeRates = rootView.findViewById(R.id.rvExchangeRates);
        pbExchangeRates = rootView.findViewById(R.id.pbExchangeRates);
        handler = new Handler();
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvExchangeRates.setLayoutManager(new LinearLayoutManager(getContext()));
        rvExchangeRates.setOnTouchListener((view1, motionEvent) -> {
            KeyboardUtils.hideKeyboard(getActivity());
            return false;
        });
    }

    @Override
    public void setExchangeRates(boolean updateFirstItem, List<ExchangeRateViewModel> data) {
        if (exchangeRatesAdapter == null) {
            exchangeRatesAdapter = new ExchangeRatesAdapter(getContext(), data, this, textWatcher);
            rvExchangeRates.setAdapter(exchangeRatesAdapter);
        } else {
            handler.post(() -> { // fix for crash: java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing a layout or scrolling
                exchangeRatesAdapter.setData(data);
                if (updateFirstItem) {
                    exchangeRatesAdapter.notifyDataSetChanged();
                } else {
                    exchangeRatesAdapter.notifyItemRangeChanged(1, exchangeRatesAdapter.getItemCount(), data);
                }

            });
        }
    }

    @Override
    public void setRefreshing(boolean isRefreshing) {
        pbExchangeRates.setVisibility(isRefreshing ? View.VISIBLE : View.GONE);
        rvExchangeRates.setVisibility(isRefreshing ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    @Override
    public void showError(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show(); // TODO: may be i should use SnackBar or AlertDialog?
    }

    @NonNull
    @Override
    protected ExchangeRatesPresenter getPresenter() {
        return (ExchangeRatesPresenter) super.getPresenter();
    }

    @Override
    public void onItemSelected(ExchangeRateViewModel item) {
        getPresenter().onItemSelected(item);
    }
}
