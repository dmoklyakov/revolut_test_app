package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.presenter;

import self.dmoklyakov.revolut_test_app.presentation.common.presenter.BasePresenter;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel.ExchangeRateViewModel;

public interface ExchangeRatesPresenter extends BasePresenter {

    void onItemSelected(ExchangeRateViewModel item);

}
