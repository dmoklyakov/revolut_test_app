package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel;

import java.math.BigDecimal;

public class ExchangeRateViewModel implements Comparable<ExchangeRateViewModel> {
    private String currencyCode;
    private BigDecimal visibleRate;
    private BigDecimal baseRate;

    public ExchangeRateViewModel(String currencyCode, BigDecimal baseRate, BigDecimal visibleRate) {
        this.currencyCode = currencyCode;
        this.baseRate = baseRate;
        this.visibleRate = visibleRate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getVisibleRate() {
        return visibleRate;
    }

    public void setVisibleRate(BigDecimal visibleRate) {
        this.visibleRate = visibleRate;
    }

    public BigDecimal getBaseRate() {
        return baseRate;
    }

    @Override
    public int compareTo(ExchangeRateViewModel another) {
        return this.getCurrencyCode().compareToIgnoreCase(another.getCurrencyCode());
    }
}
