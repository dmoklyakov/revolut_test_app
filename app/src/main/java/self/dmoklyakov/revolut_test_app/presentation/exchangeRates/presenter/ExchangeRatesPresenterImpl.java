package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.presenter;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.dmoklyakov.revolut_test_app.RevolutTestApplication;
import self.dmoklyakov.revolut_test_app.domain.repository.RevolutRepository;
import self.dmoklyakov.revolut_test_app.presentation.common.presenter.AbstractPresenter;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.view.ExchangeRatesView;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel.ExchangeRateViewModel;

public class ExchangeRatesPresenterImpl extends AbstractPresenter<ExchangeRatesView> implements ExchangeRatesPresenter {

    private static final int BIG_DECIMAL_SCALE = 4;
    private static final int BIG_DECIMAL_SCALE_TYPE = BigDecimal.ROUND_HALF_EVEN;
    private Disposable updateDisposable;
    private String baseCurrency = "EUR";
    private boolean isFirstLoad = true;
    private List<ExchangeRateViewModel> data;
    private BigDecimal baseRate = new BigDecimal(100.0);

    public ExchangeRatesPresenterImpl(@NonNull ExchangeRatesView view) {
        super(view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getView().setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    baseRate = new BigDecimal(editable.toString()).setScale(BIG_DECIMAL_SCALE, BIG_DECIMAL_SCALE_TYPE).stripTrailingZeros();
                } catch (NumberFormatException ignored) {
                    baseRate = new BigDecimal(0).setScale(BIG_DECIMAL_SCALE, BIG_DECIMAL_SCALE_TYPE).stripTrailingZeros();
                }

                for (int i = 1; i < data.size(); i++) {
                    data.get(i).setVisibleRate((data.get(i).getBaseRate().multiply(baseRate)).setScale(BIG_DECIMAL_SCALE, BIG_DECIMAL_SCALE_TYPE).stripTrailingZeros());
                }
                ExchangeRatesView view = getView();
                if (view != null) {
                    view.setExchangeRates(false, data);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        ExchangeRatesView view = getView();
        if (view != null && isFirstLoad) {
            view.setRefreshing(true);
        } else if (view != null && !isFirstLoad) {
            view.setRefreshing(false);
        }
        update();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!updateDisposable.isDisposed()) {
            updateDisposable.dispose();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemSelected(ExchangeRateViewModel item) {
        ExchangeRatesView view = getView();
        if (view == null) {
            return;
        }
        if (!updateDisposable.isDisposed()) {
            updateDisposable.dispose();
        }
        data.remove(item);
        data.add(0, item);

        baseCurrency = item.getCurrencyCode();
        baseRate = item.getVisibleRate();

        Collections.sort(data.subList(1, data.size()));
        view.setExchangeRates(true, data);
        update();
    }

    private void update() {

        updateDisposable = RevolutRepository.getInstance((RevolutTestApplication) getView().getContext().getApplicationContext())
                .getRevolutService()
                .getLatestExchangeRates(baseCurrency)
                .delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(exchangeRates -> {
                    ExchangeRatesView view = getView();
                    if (view == null) {
                        return null;
                    }
                    List<ExchangeRateViewModel> result = new ArrayList<>();
                    result.add(
                            new ExchangeRateViewModel(
                                    exchangeRates.getBase(),
                                    baseRate,
                                    baseRate.setScale(BIG_DECIMAL_SCALE, BIG_DECIMAL_SCALE_TYPE).stripTrailingZeros()
                            )
                    );
                    for (Map.Entry<String, String> entry : exchangeRates.getRates().entrySet()) {
                        result.add(
                                new ExchangeRateViewModel(
                                        entry.getKey(),
                                        new BigDecimal(entry.getValue()),
                                        baseRate.multiply(new BigDecimal(entry.getValue())).setScale(BIG_DECIMAL_SCALE, BIG_DECIMAL_SCALE_TYPE).stripTrailingZeros()
                                )
                        );
                    }
                    Collections.sort(result.subList(1, result.size()));
                    return result;
                })
                .subscribe(data -> {
                    this.data = data;
                    ExchangeRatesView view = getView();
                    if (view == null) {
                        return;
                    }
                    if (isFirstLoad) {
                        isFirstLoad = false;
                        view.setRefreshing(false);
                    }
                    view.setExchangeRates(false, ExchangeRatesPresenterImpl.this.data);
                    update();
                }, throwable -> {
                    throwable.printStackTrace();
                    ExchangeRatesView view = getView();
                    if (view != null) {
                        view.showError(throwable.getLocalizedMessage());
                    }
                    update();
                });
    }

}
