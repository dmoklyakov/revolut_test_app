package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.view;

import android.text.TextWatcher;

import java.util.List;

import self.dmoklyakov.revolut_test_app.presentation.common.view.PresenterView;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel.ExchangeRateViewModel;

public interface ExchangeRatesView extends PresenterView {

    void setExchangeRates(boolean updateFirstItem, List<ExchangeRateViewModel> data);

    void setRefreshing(boolean isRefreshing);

    void setTextWatcher(TextWatcher textWatcher);

    void showError(String text);

}
