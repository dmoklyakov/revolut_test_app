package self.dmoklyakov.revolut_test_app.presentation.common.view;

import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import self.dmoklyakov.revolut_test_app.presentation.common.presenter.BasePresenter;

public abstract class AbstractFragmentView extends Fragment implements PresenterView {

    protected BasePresenter presenter;

    @NonNull
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    @CallSuper
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().onCreate(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    @CallSuper
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }

    @Override
    @CallSuper
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        getPresenter().onSaveInstanceState(outState);
    }
}
