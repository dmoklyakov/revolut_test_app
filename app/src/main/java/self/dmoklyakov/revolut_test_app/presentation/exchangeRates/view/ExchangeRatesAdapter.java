package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.view;

import android.content.Context;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import self.dmoklyakov.revolut_test_app.R;
import self.dmoklyakov.revolut_test_app.common.utils.ResourceUtils;
import self.dmoklyakov.revolut_test_app.presentation.exchangeRates.viewModel.ExchangeRateViewModel;

public class ExchangeRatesAdapter extends RecyclerView.Adapter<ExchangeRatesAdapter.ViewHolder> {

    private List<ExchangeRateViewModel> data;
    private Context context;
    private OnItemSelectedListener onItemSelectedListener;
    private TextWatcher textWatcher;

    public ExchangeRatesAdapter(Context context, List<ExchangeRateViewModel> data, OnItemSelectedListener onItemSelectedListener, TextWatcher textWatcher) {
        this.context = context;
        this.data = data;
        this.onItemSelectedListener = onItemSelectedListener;
        this.textWatcher = textWatcher;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exchange_rate, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ExchangeRateViewModel item = data.get(holder.getAdapterPosition());

        holder.ivFlag.setImageDrawable(
                context.getResources().getDrawable(
                        ResourceUtils.getFlagResourceIdByCurrencyCode(
                                context, item.getCurrencyCode()
                        )
                )
        );
        holder.tvCurrencyCode.setText(item.getCurrencyCode());
        holder.tvCurrencyName.setText(
                ResourceUtils.getCurrencyNameResourceIdByCurrencyCode(
                        context, item.getCurrencyCode()
                )
        );
        holder.etRate.removeTextChangedListener(textWatcher);
        if (holder.getAdapterPosition() == 0) {
//            holder.etRate.setText(item.getVisibleRate().doubleValue() == 0 ? "" : item.getVisibleRate().toPlainString());
            holder.etRate.setText(item.getVisibleRate().toPlainString());
            holder.etRate.addTextChangedListener(textWatcher);
            holder.etRate.setVisibility(View.VISIBLE);
            holder.tvRate.setVisibility(View.GONE);
        } else {
//            holder.tvRate.setText(item.getVisibleRate().doubleValue() == 0 ? "" : item.getVisibleRate().toPlainString());
            holder.tvRate.setText(item.getVisibleRate().toPlainString());
            holder.etRate.setVisibility(View.GONE);
            holder.tvRate.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(view -> {
            if (holder.getAdapterPosition() > 0) {
                onItemSelectedListener.onItemSelected(item);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void setData(List<ExchangeRateViewModel> data) {
        this.data = data;
    }

    interface OnItemSelectedListener {
        void onItemSelected(ExchangeRateViewModel item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View itemView;
        ImageView ivFlag;
        TextView tvCurrencyCode;
        TextView tvCurrencyName;
        EditText etRate;
        TextView tvRate;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.ivFlag = itemView.findViewById(R.id.ivFlag);
            this.tvCurrencyCode = itemView.findViewById(R.id.tvCurrencyCode);
            this.tvCurrencyName = itemView.findViewById(R.id.tvCurrencyName);
            this.etRate = itemView.findViewById(R.id.etRate);
            this.tvRate = itemView.findViewById(R.id.tvRate);
        }
    }
}
