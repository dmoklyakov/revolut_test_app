package self.dmoklyakov.revolut_test_app.presentation.common.presenter;

import android.os.Bundle;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import self.dmoklyakov.revolut_test_app.presentation.common.view.PresenterView;

public abstract class AbstractPresenter<V extends PresenterView> implements BasePresenter {

    @NonNull
    private WeakReference<V> view;

    public AbstractPresenter(@NonNull V view) {
        this.view = new WeakReference<>(view);
    }

    public V getView() {
        return view.get();
    }

    public void setView(V view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // stub
    }

    @Override
    public void onStart() {
        // stub
    }

    @Override
    public void onStop() {
        // stub
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // stub
    }
}
