package self.dmoklyakov.revolut_test_app.presentation.exchangeRates.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import self.dmoklyakov.revolut_test_app.R;

public class ExchangeRatesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_rates);

        ExchangeRatesFragment exchangeRatesFragment = (ExchangeRatesFragment) getSupportFragmentManager().findFragmentByTag(ExchangeRatesFragment.class.getSimpleName());
        if (exchangeRatesFragment == null) {
            exchangeRatesFragment = ExchangeRatesFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.flFragmentContainer, exchangeRatesFragment, ExchangeRatesFragment.class.getSimpleName()).commit();
        }
    }
}
