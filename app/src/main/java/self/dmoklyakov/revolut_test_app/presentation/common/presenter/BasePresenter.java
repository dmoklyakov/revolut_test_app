package self.dmoklyakov.revolut_test_app.presentation.common.presenter;

import android.os.Bundle;

import androidx.annotation.Nullable;

public interface BasePresenter {

    void onCreate(@Nullable Bundle state);

    void onStart();

    void onStop();

    void onSaveInstanceState(Bundle outState);

}
